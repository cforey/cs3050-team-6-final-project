# CS3050 Team 6 Final Project
Installs 
- install Python Arcade (pip3 install arcade)
- install bezier (pip3 install bezier)
- Open the sprites folder and install the public pixel font by opening the zip file and double cllicking  to make the text display correctly 

Enemy path resources
- Bezier curve modeling tool - https://observablehq.com/@d3/spline-editor
- Enemy behavior reference - https://www.retrogamedeconstructionzone.com/2020/05/metamorphosis-from-galaxian-to-galaga.html
- Python bezier curve module - https://bezier.readthedocs.io/en/stable/python/reference/bezier.curve.html

Notes to grader: 
Instructions buiit into game just run the graphics file to run the game.
Lower power computers may experience audio issues as some of ours had audio issues when running unplugged.